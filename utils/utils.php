<?php

function route_resolve()
{
  $routes = (require __DIR__.'/../config/routes.php');

  $routePath = str_replace( '?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI'] );

  if(empty($routePath))
  {
    $routePath = '/';
  }
  else
  {
    if(strlen($routePath) > 1)
    {
      if (substr($routePath, 0, 1) == '/') {
        $routePath = substr($routePath, 1);
      }
      else if (substr($routePath, strlen($routePath)-2, 1) == '/') {
        $routePath = substr($routePath, 0, -1);
      }
    }
  }
  $callable = $routes[$routePath];
  try
  {
    if(empty($callable))
    {
      throw new \Exception("Could not find route");
    }
    $parts = explode('@', $callable);
    $className = $parts[0];
    $functionName = $parts[1];
    call_user_func_array(array((new $className), $functionName), []);
  }
  catch(\Exception $e)
  {
    http_response_code(404);
  }
}

function get_paths()
{
  return (require __DIR__.'/../config/config.php');
}
?>
