<?php

namespace Controllers;

use Core\Controller;
use Models\Inventory;
use Models\Zipcode;

class FrontController extends Controller
{
  public function home()
  {
    setcookie('zipcode', '', time() - 86400, '/');
    setcookie('distance', '', time() - 86400, '/');
    $this->render('home', ['message' => 'Home']);
  }
  public function search()
  {
    try {
      $zipcode = null;
      $distance = null;
      $page = 1;
      if(!empty($_POST['zipcode']))
      {
        $zipcode = $_POST['zipcode'];
        setcookie('zipcode', $zipcode, time() + 86400, '/');
      }
      else if(!empty($_COOKIE['zipcode']))
      {
        $zipcode = $_COOKIE['zipcode'];
      }
      if(!empty($_POST['distance']))
      {
        $distance = $_POST['distance'];
        setcookie('distance', $distance, time() + 86400, '/');
      }
      else if(!empty($_COOKIE['distance']))
      {
        $distance = $_COOKIE['distance'];
      }
      if(!empty($_GET['page']))
      {
        $page = $_GET['page'];
      }
      if(empty($zipcode) || empty($distance))
      {
        throw new \Exception('Required parameter zipcode or distance missing');
      }
      $uiConf = (require __DIR__.'/../config/ui.php');
      $inventoriesPerPage = $uiConf['inventoriesPerPage'];

      $offset = (intval($page) - 1) * $inventoriesPerPage;

      $start = (intval($page) - 1) * $inventoriesPerPage + 1;
      $end = intval($page) * $inventoriesPerPage;

      $zipcodeModel = new Zipcode();
      $zipcodeObj = $zipcodeModel->getZipcode($zipcode);

      if(empty($zipcodeObj))
      {
        throw new \Exception('Zipcode not valid');
      }

      $inventoryModel = new Inventory();
      $result = $inventoryModel->searchInventories($zipcodeObj, $distance, $inventoriesPerPage, $offset);
      $inventoryObjs = $result['inventories'];
      $count = $result['count'];
      $totalPages = ceil(intval($count) / intval($inventoriesPerPage));

      if(($page - 4) < 1)
      {
        $pageStart = 1;
        $pageEnd = $pageStart + 7;
      }
      else if(($page + 3) > $totalPages)
      {
        $pageEnd = $totalPages;
        $pageStart = $pageEnd - 7;
      }
      else
      {
        $pageStart = $page - 4;
        $pageEnd = $page + 3;
      }

      $this->render('search', [
        'inventories' => $inventoryObjs,
        'count' => $count,
        'start' => $start,
        'pageStart' => $pageStart,
        'pageEnd' => $pageEnd,
        'end' => $end,
        'totalPages' => $totalPages,
        'page' => $page,
        'zipcode' => $zipcode,
        'distance' => $distance
      ]);
    } catch (\Exception $e) {
      $this->render('error', [
        'message' => $e->getMessage()
      ]);
    }
  }
}

?>
