<?php

namespace Models;

use Core\Model;
use PDO;

class Zipcode extends Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function getZipcode($zipcode)
  {
    $stmt = $this->conn->prepare("SELECT * FROM zipcodes WHERE zipcode LIKE :zipcode LIMIT 1");
    $stmt->bindValue(':zipcode', $zipcode, PDO::PARAM_STR);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
  }
}
