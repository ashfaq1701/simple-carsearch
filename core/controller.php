<?php

namespace Core;

class Controller
{
  public function render($viewPath, $parameters)
  {
    $viewsPath = get_paths()['viewsPath'];
    extract($parameters);
    ob_start();
    require $viewsPath.'/'.$viewPath.'.php';
    $content = ob_get_clean();

    ob_start();
    require $viewsPath.'/'.'base.php';
    $fullPage = ob_get_clean();
    echo $fullPage;
  }
}

?>
