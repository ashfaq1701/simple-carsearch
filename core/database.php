<?php

namespace Core;

use PDO;

class Database
{
  public $conn;
  public static $instance = null;

  public static function getInstance()
	{
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}

  public function __construct()
  {
    $dbConf = (require __DIR__.'/../config/database.php');
    try {
      $this->conn = new PDO('mysql:host='.$dbConf['host'].';dbname='.$dbConf['database'], $dbConf['user'], $dbConf['password']);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (\Exception $e) {
      exit('Could not connect to database');
    }

  }
}

?>
