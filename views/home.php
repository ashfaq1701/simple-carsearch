<div class="jumbotron top-margin">
  <form method="POST" action="/search">
    <div class="centered">
      <div class="top-margin">
        <label class="form-label inline-block">Zip Code</label>
        <input type="text" class="form-control filter inline-block" name="zipcode" placeholder="Enter Zip Code"/>
      </div>
      <div class="top-margin">
        <label class="form-label inline-block">Distance (miles)</label>
        <input type="number" class="form-control filter inline-block" name="distance" placeholder="Enter Distance"/>
      </div>
      <div class="top-margin">
        <button type="submit" class="btn btn-success">Search</button>
      </div>
    </div>
  </form>
</div>
