<div class="jumbotron top-margin">
  <form method="POST" action="/search">
    <div class="centered">
      <div class="top-margin">
        <label class="form-label inline-block">Zip Code</label>
        <input type="text" class="form-control filter inline-block" name="zipcode" value="<?php echo $zipcode; ?>" placeholder="Enter Zip Code"/>
      </div>
      <div class="top-margin">
        <label class="form-label inline-block">Distance (miles)</label>
        <input type="number" class="form-control filter inline-block" name="distance" value="<?php echo $distance; ?>" placeholder="Enter Distance"/>
      </div>
      <div class="top-margin">
        <button type="submit" class="btn btn-success">Search</button>
      </div>
    </div>
  </form>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Year</th>
      <th>Make</th>
      <th>Model</th>
      <th>Price</th>
      <th>Dealership Name</th>
      <th>State</th>
      <th>City</th>
      <th>Zip Code</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($inventories as $inventory)
    {
    ?>
    <tr>
      <td><?php echo $inventory['inventory_year']; ?></td>
      <td><?php echo $inventory['inventory_make']; ?></td>
      <td><?php echo $inventory['inventory_model']; ?></td>
      <td>$<?php echo number_format($inventory['inventory_price']); ?></td>
      <td><?php echo $inventory['customer_name']; ?></td>
      <td><?php echo $inventory['zipcode_state']; ?></td>
      <td><?php echo $inventory['zipcode_city']; ?></td>
      <td><?php echo $inventory['zipcode']; ?></td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>

<div class="row pagination-row">
  <div class="col-md-5 bottom-margin">
    <strong>Showing <?php echo $start; ?> to <?php echo $end; ?> of total <?php echo $count; ?> entries (Page <?php echo $page; ?> of total <?php echo $totalPages?> pages)</strong>
  </div>
  <div class="col-md-1 bottom-margin">
    Jump
    <select id="page-jump">
      <?php
      for($i = 1; $i <= $totalPages; $i++)
      {
      ?>
      <option value="<?php echo $i; ?>"<?php echo $i == $page ? ' selected' : ''?>><?php echo $i; ?></option>
      <?php
      }
      ?>
    </select>
  </div>
  <div class="col-md-6 bottom-margin">
    <div class="pagination">
      <?php
      if($page != 1)
      {
      ?>
      <a href="/search?page=<?php echo $page-1; ?>">&laquo;</a>
      <?php
      }
      ?>
      <?php
      for($i = $pageStart; $i <= $pageEnd; $i++)
      {
      ?>
      <a href="/search?page=<?php echo $i; ?>"<?php echo $i == $page ? ' class="active"' : ''; ?>><?php echo $i; ?></a>
      <?php
      }
      ?>
      <?php
      if($page != $totalPages)
      {
      ?>
      <a href="/search?page=<?php echo $page+1; ?>">&raquo;</a>
      <?php
      }
      ?>
    </div>
  </div>
</div>
