<?php

function load_files($directory) {
    if(is_dir($directory)) {
        $scan = scandir($directory);
        unset($scan[0], $scan[1]);
        foreach($scan as $file) {
            if(is_dir($directory."/".$file)) {
                load_files($directory."/".$file);
            } else {
                if(strpos($file, '.php') !== false) {
                    include_once($directory."/".$file);
                }
            }
        }
    }
}
load_files('./core');
load_files('./controllers');
load_files('./utils');
load_files('./models');

?>
