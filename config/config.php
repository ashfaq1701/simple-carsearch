<?php

$basepath = __DIR__.'/..';

return [
  'basepath' => $basepath,
  'controllersPath' => $basepath.'/controllers',
  'modelsPath' => $basepath.'/models',
  'viewsPath' => $basepath.'/views',
  'assetsPath' => $basepath.'/assets'
];

?>
